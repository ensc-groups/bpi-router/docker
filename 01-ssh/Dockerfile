## TODO: move this into Ubuntu/Debian Dockerfiles; for now, keep it
## here because only this file is mangled by the 'build' script
RUN \
	if test -x /usr/sbin/locale-gen; then \
		/usr/sbin/locale-gen '@LANG@' '@LANG_PREFIX@'; \
	fi

RUN \
	/usr/sbin/groupadd -g @UID@ @USER@ && \
	/usr/sbin/useradd -u @UID@ -g @USER@ -m -s /bin/bash @USER@ && \
	id @USER@ && \
	mkdir -m 0711 /home/@USER@/.ssh && \
	chown @USER@:@USER@ /home/@USER@/.ssh && \
	:

USER @UID@:@UID@

RUN \
	HOME=~@USER@; export HOME && \
	D=~@USER@/bin && \
	mkdir -p "$D" && \
	git config --global user.email "@EMAIL@" && \
	git config --global user.name "@USERNAME@"

USER 0:0

COPY profile.d			/etc/profile.d/
COPY authorized_keys.target	/home/@USER@/.config/openembedded/authorized_keys
COPY local.mk			/home/@USER@/.config/openembedded/local.mk
COPY local.conf			/home/@USER@/.config/openembedded/local.conf
COPY ssh_config			/home/@USER@/.ssh/config
COPY authorized_keys.docker	/home/@USER@/.ssh/authorized_keys
COPY authorized_keys.docker	/root/.ssh/authorized_keys
COPY bin			/usr/local/bin

COPY project-bblayers.conf      /home/@USER@/.config/openembedded/project-bblayers.conf
COPY project-local.conf         /home/@USER@/.config/openembedded/project-local.conf

COPY rauc.key			/home/@USER@/.local/share/@PROJECT@-ca/rauc/rauc.key
COPY rauc.crt			/home/@USER@/.local/share/@PROJECT@-ca/rauc/rauc.crt

COPY image.key			/home/@USER@/.local/share/@PROJECT@-ca/image/image.key
COPY image.crt			/home/@USER@/.local/share/@PROJECT@-ca/image/image.crt

ADD  http://dl.sigma-chemnitz.de/elito/sources/flymake-kernel.mk \
     /home/@USER@/lib/make/flymake-kernel.mk

ADD  http://dl.sigma-chemnitz.de/elito/sources/bitbake-conf.sample \
     /home/@USER@/.config/openembedded/bitbake.conf

RUN \
	chmod 0644 /root/.ssh/authorized_keys && \
	ln -s openembedded /home/@USER@/.config/oe && \
	chown -R @USER@:@USER@ /home/@USER@ && \
	chmod 0644 /home/@USER@/.ssh/authorized_keys && \
	chmod 0600 /home/@USER@/.ssh/config && \
	chmod 0600 /home/@USER@/.local/share/@PROJECT@-ca/rauc/rauc.key && \
	chmod 0600 /home/@USER@/.local/share/@PROJECT@-ca/image/image.key && \
	chmod 0755 /usr/local/bin/* && \
	:

RUN \
	mkdir /etc/supervise.d && \
	mkdir /etc/supervise.d/sshd /etc/supervise.d/httpd && \
	printf "#! /bin/sh\nexec /usr/sbin/sshd -De\n" > /etc/supervise.d/sshd/run && \
	printf "#!/bin/sh\ntest -d '@WORKSPACE_DIR@/www' || sleep 60\nexec /sbin/busybox httpd -f -p 8080 -u ftp -vv -h '%s/www'\n" \
		"@WORKSPACE_DIR@" "@WORKSPACE_DIR@" > /etc/supervise.d/httpd/run && \
	chmod a+x /etc/supervise.d/*/run

USER @UID@:@UID@

RUN \
	cd /home/@USER@ && \
	git config --global --add core.gitProxy 'none for sigma-chemnitz.de' && \
	git config --global --add core.gitProxy 'none for git.andto.de' && \
	git config --global --add core.gitProxy /usr/local/bin/git-tunnel && \
	git config --global --add 'include.path' '~/.gitconfig.mirrors' && \
	git config --global --add 'include.path' '~/.gitconfig.mirrors-ssh' && \
	git config --global --add 'include.path' '/etc/gitconfig.mirrors' && \
	git config --global --add 'include.path' '/etc/gitconfig.mirrors-ssh' && \
	:

USER 0:0

VOLUME	/var/tmp
VOLUME	/var/cache

@USE_BUSYBOX@ ENTRYPOINT ["@BUSYBOX_INIT@", "runsvdir", "/etc/supervise.d"]
@USE_SSHD@ ENTRYPOINT ["/usr/sbin/sshd", "-D"]
