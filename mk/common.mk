HOSTNAME	= $(firstword $(shell hostname -i | sed 's![^ ]*:[^ ]*[[:space:]]!!g'))
BUILDHOST	?= docker.${PROJECT}
REMOTE		= ssh -t "${BUILDHOST}"
REMOTE_MAKE	= ${REMOTE} ${MAKE}
ORIG_MAKE	= ${MAKE} -f $(firstword $(MAKEFILE_LIST))

TOPDIR         ?= <unset>
BASEDIR        ?= <unset>
WS             ?= <unset>

SHELL		= bash

include ${HOME}/.config/elito/do-setup.mk

DFLT_TARGETS	?= build-kernel build-dtree

CONFIGS_REPO	?= ${WS}/.configs

DOCKER_PORT	?= ${DOCKER_PORT_${PROJECT}}
KERNEL_PORT	?= ${KERNEL_PORT_${PROJECT}}
DTREE_PORT	?= ${DTREE_PORT_${PROJECT}}
UBOOT_PORT	?= ${UBOOT_PORT_${PROJECT}}
BAREBOX_PORT	?= ${BAREBOX_PORT_${PROJECT}}
SCREEN_NUMBERS	?= ${SCREENS_${PROJECT}}

BAREBOX_ARCH	?= ${KERNEL_ARCH}

PAGES_URI	?= http://pages.example.com
PAGES_IMAGE_URI ?= ${PAGES_URI}/latest/image

DEPLOY_IMAGE_DIR = ${DEPLOY_DIR}/images/${MACHINE}
SDCARD_SUFFIX	?= .xz
SDCARD_BASE	?= ${IMAGE}-${MACHINE}.wic${SDCARD_SUFFIX}
SDCARD_IMAGE	?= ${DEPLOY_IMAGE_DIR}/${SDCARD_BASE}
SDCARD_DEV	?= /dev/sdcard
SDCARD_DD	?= dd

CURL		?= curl

## run in clean environment; e.g. "git rebase -x 'do-XXXX ...'" might
## cause commands to run in the wrong git repository
GIT		?= env -i PATH="$${PATH}" git

_SCREEN_SSH ?=		$(if ${SCREEN_NUMBERS},$(word 1,${SCREEN_NUMBERS}),10)
_SCREEN_RECEIVE ?=	100

UNCOMPRESS_CAT = \
	$(if $(filter %.xz,$1),xzcat,\
	$(if $(filter %.bz,$1),bzcat,\
	$(if $(filter %.gz,$1),zcat,\
	cat))) '$1'

REMOTE_MAKE_FLAGS = \
	HOST_IP='${HOSTNAME}' \
	MACHINE='${MACHINE}' \
	DEPLOY_DIR='${DEPLOY_DIR}' \
	IMAGE_RECIPE='${IMAGE}' \

MAKEFILE_KERNEL  ?= ${WS}/Makefile.kernel
MAKEFLAGS_KERNEL ?= \
	KBUILD_OUTPUT= \
	$(if ${KENREL_PORT},KERNEL_TFTP_IMAGE='/dev/tcp/${HOSTNAME}/${KERNEL_PORT}') \
	$(if ${KERNEL_V},KERNEL_V=${KERNEL_V}) \
	$(if ${KERNEL_BUILD_DIR},O=${KERNEL_BUILD_DIR}) \
	$(if ${KERNEL_NFSSERVER},KERNEL_NFSSERVER='${KERNEL_NFSSERVER}') \
	$(if ${KERNEL_ARCH},KERNEL_ARCH='${KERNEL_ARCH}') \
	IMAGEDIR="${DEPLOY_IMAGE_DIR}" \
	$T

MAKEFILE_BAREBOX ?= ${WS}/Makefile.barebox
MAKEFLAGS_BAREBOX ?= \
	KBUILD_OUTPUT= \
	$(if ${BAREBOX_V},BAREBOX_V=${BAREBOX_V}) \
	$(if ${BAREBOX_PORT},KERNEL_TFTP_IMAGE='/dev/tcp/${HOSTNAME}/${BAREBOX_PORT}') \
	$(if ${BAREBOX_BUILD_DIR},O=${BAREBOX_BUILD_DIR}) \
	$(if ${BAREBOX_IMAGE},BAREBOX_IMAGE=${BAREBOX_IMAGE}) \
	$(if ${BAREBOX_ARCH},KERNEL_ARCH='${BAREBOX_ARCH}') \
	$T

MAKEFILE_UBOOT ?= ${WS}/Makefile.u-boot
MAKEFLAGS_UBOOT ?= \
	$(if ${UBOOT_V},UBOOT_V=${UBOOT_V}) \
	$(if ${UBOOT_PORT},KERNEL_TFTP_IMAGE='/dev/tcp/${HOSTNAME}/${UBOOT_PORT}') \
	$(if ${UBOOT_BUILD_DIR},O=${UBOOT_BUILD_DIR}) \
	$T

DTREE_FULL_PATH ?= ${WS}/.dtb$(if ${KERNEL_V},/${KERNEL_V})
MAKEFILE_DTREE ?= _scratch.mk
MAKEFLAGS_DTREE	?= \
	$(if ${KERNEL_V},KERNEL_V=${KERNEL_V}) \
	$(if ${KERNEL_DIR},KERNEL_DIR=${KERNEL_DIR}) \
	$(if ${KERNEL_BUILD_DIR},KERNEL_BUILD_DIR=${KERNEL_BUILD_DIR}) \
	$(if ${KERNEL_ARCH},KERNEL_ARCH='${KERNEL_ARCH}') \
	WS='${WS}' \
	$T \
	'&&' \
	$(if ${DTREE_PORT},cat ${DTREE_FULL_PATH}/${DTREE_FILE} '>' /dev/tcp/${HOSTNAME}/${DTREE_PORT},:)

KMOD_DIR ?= $(if ${KERNEL_BUILD_DIR},${KERNEL_BUILD_DIR},${KERNEL_DIR})
MAKEFILE_KMOD ?= ${MAKEFILE_KERNEL}
MAKEFLAGS_KMOD ?= \
	M='$M' \
	IS_KMOD=1 \
	$T

# usage $(call EXEC,extra-title,cmd)
define EXEC
@! tty -s || test x"$$TERM" = xdumb || echo -ne "\033]0;do-${PROJECT}: $@$1\007"
$2
endef

# usage $(call EXEC_SCREEN,number,title,cmd)
define EXEC_SCREEN
$(if $1,,@echo "Missing SCREEN setup" >&2; exit 1;)
@test -n "$${STY}" || { echo "screen not active" >&2; exit 1; }
screen -X screen -t '$2' $1 $3
endef

# usage: $(call remote_make,$TYPE)
remote_make = $(call EXEC,,${REMOTE_MAKE} \
	-C '${$1_DIR}' \
	-f $(if $2,'${WS}/Makefile.common','${MAKEFILE_$1}') \
	$(if $2,R=$2) \
	${REMOTE_MAKE_FLAGS} ${MAKEFLAGS_$1})

# usage: $(call local_make,$TYPE)
### NOTE: try to provide a clean environment by unsetting commonly
### used variables and unset MAKEOVERRIDES
local_make = $(call EXEC,,env -u F -u T ${MAKE} \
	-C '${$1_DIR}' \
	-f $(if $2,'${WS}/Makefile.common','${MAKEFILE_$1}') \
	MAKEOVERRIDES= \
	$(if $2,R=$2) \
	${REMOTE_MAKE_FLAGS} ${MAKEFLAGS_$1})

# usage: $(call common_make,$RECIPE[,$DIR])
common_make = $(call EXEC,,${REMOTE_MAKE} \
	-C '${WS}/$(if $2,$2,$1)' \
	-f '${WS}/Makefile.common') \
	S='${WS}/$(if $S,$S,$1)' \
	R=$1 \
	${REMOTE_MAKE_FLAGS} \
	$3 \
	$T

__copy_config = \
	$(if $@,@echo '  COPY_CONFIG $1';,)\
	if test -d '$2' -a -s '$1'; then \
		rm -f '$2/$3' && \
		cp '$1' '$2/$3' && \
		${GIT} -C '$2' add -- '$3' && { \
			${GIT} -C '$2' diff-index --quiet HEAD -- '$3' || \
			${GIT} -C '$2' commit -q -m 'update' -- '$3'; \
		}; \
	fi

_copy_config = $(call __copy_config,$1/.config,$2,$(notdir $1))

copy_config = $(call _copy_config,${$1_BUILD_DIR},${CONFIGS_REPO})

######

all:

## The 'receive-*' rules
receive-all:	FORCE
	${SOCK_REDIR} ${SOCK_REDIR_FLAGS}

screen-receive:	FORCE
	$(call EXEC_SCREEN,${_SCREEN_RECEIVE},receive-all (${PROJECT}),${ORIG_MAKE} receive-all)

screen-ssh:	FORCE
	$(call EXEC_SCREEN,${_SCREEN_SSH},ssh (${PROJECT}),ssh ${BUILDHOST})

## generic build rules

ifeq (${NO_GENERIC_RULES},)

build-kernel:	T=tftp
build-kernel:	FORCE
	$(call remote_make,KERNEL)
	$(call copy_config,KERNEL)

build-barebox:	T=$(if $(BAREBOX_PORT),tftp)
build-barebox:	FORCE
	$(call remote_make,BAREBOX)
	$(call copy_config,BAREBOX)

build-uboot:	T=$(if $(UBOOT_PORT),tftp)
build-uboot:	FORCE
	$(call remote_make,UBOOT)
	$(call copy_config,UBOOT)

build-dtree:	FORCE
	$(call remote_make,DTREE)

kmod-%:		T=modules_install
kmod-%:		M=${WS}/kmod-$*
kmod-%:
	$(call remote_make,KMOD)

endif				# NO_GENERIC_RULES

####

_sdcard_cat = \
	$(if $(filter %.xz,$1),xzcat,\
	$(if $(filter %.gz,$1),zcat,\
	$(if $(filter %.bz2,$1),bzcat,\
	cat))) '$1'

.write-sdcard:	${SDCARD_IMAGE}
.write-deploy:	${TMPDIR}/deploy/${SDCARD_IMAGE}

.write-deploy .write-sdcard:
	$(call _sdcard_cat,$<) | ${SDCARD_DD} of='${SDCARD_DEV}' status=progress conv=fsync bs=1M iflag=fullblock

write-deploy:	FORCE
	+${Q}t=`mktemp -d /var/tmp/${PROJECT}-dl.XXXXXX` && trap "rm -rf $$t" EXIT && \
	${ORIG_MAKE} -C $$t '.$@' TMPDIR=$$t SDCARD_IMAGE='$(notdir ${SDCARD_IMAGE})'

write-sdcard:	FORCE
	+${Q}${ORIG_MAKE} '.$@' SDCARD_IMAGE='${SDCARD_IMAGE}'

${TMPDIR}/deploy/${SDCARD_IMAGE}:
	${Q}rm -f '$@'
	${Q}mkdir -p '${@D}'
	${CURL} --output '$@' -fsS '${PAGES_IMAGE_URI}/${@F}'

info:	FORCE
	@printf "Environment:\n"
	@printf "  %20s: %s\n" "PROJECT"   '${PROJECT}'
	@printf "  %20s: %s\n" "BUILDHOST" '${BUILDHOST}'
	@printf "  %20s: %s\n" "DOCKER_PORT" '${DOCKER_PORT}'
	@printf "  %20s: %s\n" "MACHINE"   '${MACHINE}'
	@printf "  %20s: %s\n" "IMAGE"     '${IMAGE}'
	@printf "Paths:\n"
	@printf "  %20s: %s\n" "TOPDIR"    '${TOPDIR}'
	@printf "  %20s: %s\n" "BASE_DIR"  '${BASE_DIR}'
	@printf "  %20s: %s\n" "DEPLOYDIR" '${DEPLOY_DIR}'
	@printf "  %20s: %s\n" "IMAGEDIR"  '${DEPLOY_IMAGE_DIR}'
	@printf "Misc:\n"
	@printf "  %20s: %s\n" "WS"        '${WS}'
	@$(if ${DTREE_DIR}, printf "  %20s: %s\n" "DTREE_DIR"        '${DTREE_DIR}')
	@$(if ${KERNEL_DIR},printf "  %20s: %s\n" "KERNEL_DIR"       '${KERNEL_DIR}')
	@$(if ${PAGES_URI}, printf "  %20s: %s\n" "PAGES_URI"        '${PAGES_URI}')

emit-var_%: FORCE
	@echo '${$*}'


FORCE:
.PHONY:	FORCE

###########

TFTP_DIR	?= /var/lib/tftpboot
SOCK_REDIR_IP	?= 0.0.0.0
SOCK_REDIR	= env RUST_LOG=info sock-redir
SOCK_REDIR_FLAGS ?= \
	--title '${PROJECT}: $@' \

define _tftp_map
SOCK_REDIR_FLAGS += -l '$${SOCK_REDIR_IP}:$${$1_PORT}>$${TFTP_DIR}/$$(strip $2)'
endef

tftp_map = $(eval $(call _tftp_map,$1,$2))

###########

ifneq (${V},)
Q =
else
Q = @
ORIG_MAKE += --no-print-directory
endif

ifneq ($(findstring k,$(firstword -$(MAKEFLAGS))),)
REMOTE_MAKE += -k
endif

ifneq ($(findstring s,$(firstword -$(MAKEFLAGS))),)
REMOTE_MAKE += -s
endif

ifneq ($(findstring B,$(firstword -$(MAKEFLAGS))),)
REMOTE_MAKE += -B
endif

MAKE_SMP_FLAGS ?= $(shell a=`getconf _NPROCESSORS_ONLN` && printf -- '-j%u' $$(( a * 150 / 100 )))
REMOTE_MAKE += ${MAKE_SMP_FLAGS}

###########

.NOTPARALLEL:
.SECONDEXPANSION:

## The 'build-*' rules
build-all:	$${DFLT_TARGETS}

###########

## https://docs.gitlab.com/ce/user/project/push_options.html#push-options-for-merge-requests
GITLAB_MERGE_OPTS =\
	-o merge_request.create \
	-o merge_request.target='${MERGE_TARGET}' \
	-o merge_request.merge_when_pipeline_succeeds \
	-o merge_request.remove_source_branch \

GITLAB_FASTBUILD_OPTS = \
	-o ci.variable=X_CI_NO_SDK=t \
	-o ci.variable=X_CI_NO_DEPLOY=t \

gitlab-merge:
	@test -n '${MERGE_BRANCH}' || { echo '*** Missing MERGE_BRANCH'; exit 1; }
	@test -n '${MERGE_TARGET}' || { echo '*** Missing MERGE_TARGET'; exit 1; }
	${GIT} push gitlab -f 'HEAD:${MERGE_BRANCH}' ${GITLAB_MERGE_OPTS}

gitlab-testbuild:
	${GIT} push gitlab HEAD ${GITLAB_FASTBUILD_OPTS}

gitlab-help:
	@echo "Targets:"
	@echo "  gitlab-merge \\"
	@echo "    MERGE_BRANCH=<branch-to-push>       \\ # e.g. MERGE_BRANCH=feature/foo"
	@echo "    MERGE_TARGET=<branch-to-merge-into>   # e.g. MERGE_TARGET=develop"

###########

DSSH		?= dssh
TARGET_IP_0 ?=	${TARGET_IP}

ssh:	ssh.0

ssh.%:	FORCE
	${DSSH} ${TARGET_IP_$*}
