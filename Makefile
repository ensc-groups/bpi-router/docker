P = 22430
PREFIX = bpi-router
DISTRO = fedora

PODMAN = podman

all:

build:	.build-${DISTRO}
run:	.run-${DISTRO}
rm:	.rm-${DISTRO}

build-slave:
	./bin/build-slave fedora - $P

.build-%:
	./bin/build "$*" - $P

.rm-%:
	-${PODMAN} stop '${PREFIX}_$*'
	-${PODMAN} rm -f '${PREFIX}_$*'

.run-%:	.rm-%
	./bin/run '$*' - $P

### rauc

RAUC_REQ_SUBJ = /C=DE/L=Chemnitz/O=SIGMA Chemnitz GmbH/OU=developer rauc ca (${PREFIX})
RAUC_REQ_FLAGS = \
	-x509 \
	-newkey rsa:4096 \
	-days 365 \
	-nodes \
	-subj '${RAUC_REQ_SUBJ}' \
	-addext 'basicConstraints=CA:FALSE' \
	-addext 'subjectKeyIdentifier=hash' \
	-addext 'authorityKeyIdentifier=keyid:always,issuer:always' \

.build-${DISTRO}:	01-ssh/rauc.crt 01-ssh/rauc.key

01-ssh/rauc.crt 01-ssh/rauc.key:	01-ssh/.rauc.stamp

%/.rauc.stamp:
	rm -f ${@D}/rauc.crt ${@D}/rauc.key
	openssl req ${RAUC_REQ_FLAGS} -keyout ${@D}/rauc.key -out ${@D}/rauc.crt
	@touch $@

### image signing

IMAGE_REQ_SUBJ = /C=DE/L=Chemnitz/O=SIGMA Chemnitz GmbH/OU=developer image ca (${PREFIX})
IMAGE_REQ_FLAGS = \
	-x509 \
	-newkey rsa:4096 \
	-days 365 \
	-nodes \
	-subj '${IMAGE_REQ_SUBJ}' \
	-addext 'basicConstraints=CA:FALSE' \
	-addext 'subjectKeyIdentifier=hash' \
	-addext 'authorityKeyIdentifier=keyid:always,issuer:always' \

.build-${DISTRO}:	01-ssh/image.crt 01-ssh/image.key

01-ssh/image.crt 01-ssh/image.key:	01-ssh/.image.stamp

%/.image.stamp:
	rm -f ${@D}/image.crt ${@D}/image.key
	openssl req ${IMAGE_REQ_FLAGS} -keyout ${@D}/image.key -out ${@D}/image.crt
	@touch $@

### common

FORCE:
.PHONY:	FORCE
